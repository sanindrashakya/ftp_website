# This is a sample build configuration for PHP.
# Check our guides at https://confluence.atlassian.com/x/e8YWN for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: aariacarterweir/lamp-gitftp:latest

pipelines:
  custom: # Pipelines that are triggered manually via the Bitbucket GUI
    init: # -- First time init
    - step:
        caches:
          - composer
        script:
          - apt-get update
          - apt-get -qq install git-ftp
          - git ftp init -u "$FTP_USERNAME" -p "$FTP_PASSWORD" ftp://$FTP_HOST
    deploy-all: # -- Deploys all files from the selected commit
    - step:
        caches:
          - composer
        script:
          - apt-get update
          - apt-get -qq install git-ftp
          - git ftp push -u "$FTP_USERNAME" -p "$FTP_PASSWORD" ftp://$FTP_HOST --all
    sonar:
    - step:
       script:
          - echo "Manual Triggers for sonar"
          - vendor/bin/phpstan analyze -c phpstan.neon.dist --no-interaction --no-progress
          - vendor/bin/phpunit -c phpunit.xml.dist --coverage-clover=coverage-report.clover --log-junit=test-report.xml
          - sonar-scanner   
  branches: # Automated triggers on commits to branches
    master: # -- When committing to master branch
    - step:
        deployment: production
        caches:
          - composer
        script:
          - apt-get update
          - apt-get -qq install git-ftp
          - git ftp push -u "$FTP_USERNAME" -p "$FTP_PASSWORD" ftp://$FTP_HOST